import winreg as wr

__all__ = ("create_new", "python_clear")
__version__ = "1.0"
__author__ = "DevLink42"
__doc__ = "see README.md"


class Reg():
    def __init__(self, value, subkey, key = wr.HKEY_CLASSES_ROOT): self.value, self.subkey, self.key = value, subkey, key

    def delete(self):
        try:
            try: k = wr.OpenKey(self.key, self.subkey, 0, wr.KEY_ALL_ACCESS)
            except: k = wr.OpenKey(self.key, self.subkey)
            try:
                while True: Reg("", self.subkey + "\\" + wr.EnumKey(k, 0), self.key).delete()
            except: wr.CloseKey(k)         
            return wr.DeleteKey(self.key, self.subkey) or True
        except: return False

    def del_val(self):
        try: return wr.DeleteValue(wr.OpenKey(self.key, self.subkey, 0, wr.KEY_ALL_ACCESS), self.value) or True
        except:           
            try: return wr.DeleteValue(wr.OpenKey(self.key, self.subkey), self.value) or True
            except: return False

    def read(self, default = ""):
        try: return str(wr.QueryValueEx(wr.OpenKey(self.key, self.subkey, 0, wr.KEY_ALL_ACCESS), self.value)[0])
        except:
            try: return str(wr.QueryValue(wr.OpenKey(self.key, self.subkey, 0, wr.KEY_ALL_ACCESS), self.value)[0])
            except: 
                try: return str(wr.QueryValueEx(wr.OpenKey(self.key, self.subkey), self.value)[0])
                except:
                    try: return str(wr.QueryValue(wr.OpenKey(self.key, self.subkey), self.value)[0])
                    except: return default

    def write(self, data = "", type = wr.REG_SZ, access = wr.KEY_WRITE):
        try: return wr.SetValueEx(wr.CreateKeyEx(self.key, self.subkey, 0, access), self.value, 0, type, data) or True
        except: return False


def create_new(ext: str):
    """Requires to have launched the program as administrator (easily done with asadmin.cmd)
    example : create_new("py")"""
    return Reg("NullFile", f".{ext}\\ShellNew").write()

def python_clear():
    """Requires to have launched the program as administrator (easily done with asadmin.cmd)"""
    icon = Reg("", r"Python.File\DefaultIcon").write(r'"C:\WINDOWS\py.exe",2')
    idle = Reg("Subcommands", r"Python.File\Shell\editwithidle").del_val(), Reg("", r"Python.File\Shell\editwithidle\command").write(Reg("", r"Python.File\Shell\editwithidle\shell\edit39\command").read())
    return icon, idle


while "main" in __name__:
    enter = int(input("Requires to have launched the program as administrator (easily done with asadmin.cmd) \n0 = create_new and 1 = python_clear : "))
    if enter == 0:
        ext = input("extension : ")
        print(create_new(ext))
    elif enter == 1:
        print(python_clear())
