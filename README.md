# Add file extension in "New" in the Windows menu when you right click in a folder or other

- Need Python 3.6 or later to run this program and Windows Vista or later. Only tested on Windows 10 with Python 3.9.

If you don't know exactly what you are doing, be careful, the registry is not to be taken lightly, it can cause problems (especially for `python_clear()`). If you run this program without administrator rights you may not have any redirection to IDLE when you right click on a `.py` file. But for `create_new("extension")` no real problem can occur with or without administrator permissions.
